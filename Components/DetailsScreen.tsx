import React from 'react'
import { View, Text, Button } from 'react-native'
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

import { RootStackParamList } from '../types';

type Props = NativeStackScreenProps<RootStackParamList, 'Details'>

const DetailsScreen = ({ navigation }: Props) => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>DetailsScreen</Text>
        <Button title="Go to Details... again" 
            onPress={() => navigation.push('Details')}
        />
        <Button title="Go to Home" onPress={() => navigation.navigate('Home')}/>
        <Button title="Go Back" onPress={() => navigation.goBack()} />
    </View>
  )
}

export default DetailsScreen